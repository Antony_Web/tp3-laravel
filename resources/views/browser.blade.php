<!-- PAGE NAVIGATION DES DONATIONS -->

@extends('layouts.app')

@section('page_title')
{{"Donation Browser"}}
@endsection

@section('content')

<div id="container">
    <main class="container-md" id="main">

                <div class="mt-5">
                    <article class="box text-white" style="width: auto;">
                        <h1 class="fw-bolder">Donation browser</h1>
                        <x-success-message class="mb-4 mt-2" />
                    </article>

                    <section class="row justify-content-evenly mt-5 mx-sm-3">

                        @foreach ($donations as $donation) <!-- POUR CHAQUE DONATION, AFFICHER (uniquement les non-réservés) -->
                        @if($donation->is_reserved != 1)

                        <article class="box col-md-5 col-sm-12 row p-3 mt-md-3 mt-sm-5 d-flex justify-content-around">
                            <img src="/storage/images/{{ $donation->image }}" alt="image" class="col" style="border-radius: 100%; max-height: 200px; max-width: 200px;">
                            
                            <div class="col">
                                <p style="text-align: justify;">{{ Str::limit($donation->description, 80) }}</p>
                                <div class="d-flex justify-content-start flex-column text-left" style="width: auto; height: 100px;">
                                    <p>Meteo: Cold</p>
                                    <div class="row justify-content-between">
                                        <form class="col-5" action="{{ route('donation.reserve', $donation->id) }}" method="POST">
                                            @method('PUT')
                                            @csrf
                                            <input type="number" name="reserve" id="reserve" value="1" hidden>
                                            <button type="submit" class="btn yellowBtn">reserve</button>
                                        </form>
                                        <a class="col-5 btn greenBtn" href="{{route('view-item', $donation->id)}}">View</a>
                                    </div>
                                </div>
                            </div>
                        </article>
                        @endif
                        @endforeach

                        <div class="d-flex justify-content-center mt-5">
                            {{ $donations->links() }}
                        </div>
                    </section>
                </div>
    </main>
</div>

@endsection