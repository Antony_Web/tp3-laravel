<nav class="navbar navbar-expand-lg">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Laravel TP3</a>
    <button class="navbar-toggler text-white" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <i class="bi bi-list"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
    <div class="mx-auto"></div>
      <ul class="navbar-nav">
        
        @if (Route::has('login'))
            
            @auth
            <li class="nav-item">
              <a class="nav-link" href="/browser">Browser</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/add-donation">Add a donation</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/profile">My profile</a>
            </li>

            <li class="nav-item">
                <div class="nav-link fw-bolder" style="color: #ffd754; border-style: solid; border-color: white; border-width: 2px; border-radius: 10px;">Hello, {{ Auth::user()->name }}!</div>
            </li>

            <li class="nav-item">

              <form method="POST" action="{{ route('logout') }}">
                              @csrf
                              <a class="nav-link redBtn"  href="{{ route('logout') }}" 
                                onclick="event.preventDefault();
                                  this.closest('form').submit();">
                                  {{ __('Log Out') }}
                                </a>
                          </form>
            </li>
            
            @else

            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="/">Home</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}" >Register</a>
                </li>
                @endif
            @endauth
        @endif
      </ul>
    </div>
  </div>
</nav>