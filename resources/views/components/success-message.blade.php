@if (session('message'))
    <div {{ $attributes }}>
        <div class="alert alert-success" role="alert">
            {{ __('Success!') }}
        </div>

        <div class="mt-3 text-center" style="color: #93db7f;">
                <h6>{{ session('message') }}</h6>
        </div>
    </div>
@endif
