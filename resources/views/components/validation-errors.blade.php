@props(['errors'])

@if ($errors->any())
    <div {{ $attributes }}>
        <div class="alert alert-danger" role="alert">
            {{ __('Hmm... something went wrong!') }}
        </div>

        <div class="mt-3 text-center" style="color: #ff837a;">
            @foreach ($errors->all() as $error)
                <h6>{{ $error }}</h6>
            @endforeach
        </div>
    </div>
@endif
