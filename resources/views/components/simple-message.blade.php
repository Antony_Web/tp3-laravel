@if (session('message'))
    <div {{ $attributes }}>
        <div class="alert alert-secondary" role="alert">
            {{ session('message') }}
        </div>
    </div>
@endif
