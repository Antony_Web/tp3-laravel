<!-- PAGE PROFILE -->

@extends('layouts.app')

@section('page_title')
{{"Your Profile"}}
@endsection

@section('content')

<div id="container" class="container-sm d-flex justify-content-center">

    <main id="main" class="row">

        <section class="col" style="margin-top: 100px; width:100px">
            <div class="box p-5">

                <h1 class="fw-bolder">Your profile</h1>

                <!-- Validation Errors -->
                <x-validation-errors class="mb-4" :errors="$errors" />
                
                <x-success-message class="mb-4" />

                <form action="{{ route('profile.update') }}" method="POST">
                    @method('PUT')
                    @csrf
                    <!-- Username -->
                    <div class="mt-4">
                        <label class="form-label" for="name">Your name</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{ auth()->user()->name }}" required autofocus>
                    </div>
                    <!-- Email -->
                    <div class="mt-4">
                        <label class="form-label" for="email">Your Email</label>
                        <input class="form-control" type="email" name="email" id="email" value="{{ auth()->user()->email }}" required>
                    </div>
                    <!-- Password -->
                    <div class="mt-4">
                        <label class="form-label" for="new-password">New password</label>
                        <input class="form-control" type="password" name="password" id="new-password" required autocomplete="new-password">
                    </div>
                    <!-- Password confirmation -->
                    <div class="mt-4">
                        <label class="form-label" for="password_confirmation">Confirm your password</label>
                        <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" required autocomplete="confirm-password">
                    </div>
                    <!-- Adress -->
                    <div class="mt-4">
                        <label class="form-label" for="adress">Your local adress</label>
                        <input class="form-control" type="text" name="adress" id="adress" value="{{ auth()->user()->adress }}" required>
                    </div>
                    <!-- City -->
                    <div class="mt-4">
                        <label class="form-label" for="city">Town/City region</label>
                        <input class="form-control" type="text" name="city" id="city" value="{{ auth()->user()->city }}" required>
                    </div>

                    
                    <div class="row mt-5 d-flex justify-content-around">
                        <button type="submit" class="col-12 btn greenBtn py-3 my-sm-2">Update</button>
                    </div>
                </form>
            </div>

        </section>

        <section class="row justify-content-evenly mt-5 mx-sm-3">
            @if($reserved) <!-- SI UNE DONATION EST RÉSERVÉ PAR LE USER, L'AFFICHER.-->
                        <article class="box col-md-5 col-sm-12 row p-3 mt-md-3 mt-sm-5 d-flex justify-content-around">
                        <img src="/storage/images/{{ $reserved->image }}" alt="image" class="col" style="border-radius: 100%; max-height: 200px; max-width: 200px;">
                            <div class="col">
                                <h5>Item ID: {{ $reserved->id}}</h5>
                                <p style="text-align: justify;">{{ Str::limit($reserved->description, 80) }}</p>
                                <div class="d-flex justify-content-start flex-column text-left" style="width: auto; height: 100px;">
                                    <div class="row justify-content-between">
                                        <form class="col-5" action="{{ route('item.confirm', $reserved->id) }}" method="POST">
                                            @method('DELETE')
                                            @csrf 
                                            <button type="submit" class="btn greenBtn">Confirm</button>
                                        </form>

                                    </div>
                                    
                                </div>
                                
                            </div>
                        </article>
                        @elseif (! $reserved)
                        <h2 class="text-white text-center">You have nothing reserved currently.</h2>
            @endif
                    </section>
    </main>
</div>
@endsection