@extends('layouts.app')

@section('page_title')
{{"Login"}}
@endsection

@section('content')
<div id="container" class="container-sm d-flex justify-content-center">

    <main id="main" class="row">

        <section class="col" style="margin-top: 30%; width:450px">
            <div class="box p-5">

                <h1 class="fw-bolder">Login</h1>

                <x-validation-errors class="mb-4" :errors="$errors" />

                <form class="mt-5" action="{{ route('login') }}" method="POST">
                    @csrf
                    <!-- Email -->
                    <div class="mt-4">
                        <label class="form-label" for="username">Email</label>
                        <input class="form-control" type="email" name="email" id="email" value="{{ old('email') }}" required autofocus>
                    </div>
                    <!-- Password -->
                    <div class="mt-4">
                        <label class="form-label" for="username">Password</label>
                        <input class="form-control" type="password" name="password" id="password" value="{{ old('password') }}" required autocomplete="current-password">
                    </div>

                    
                    <div class="row mt-5 d-flex justify-content-around">
                        <button type="submit" class="col-md-5 col-sm-12 btn greenBtn mt-5 py-3 my-sm-2">Login</button>
                        <a class="col-md-5 col-sm-12 btn yellowBtn py-3 my-sm-2" href="{{ route('register') }}" >Sign up!</a>
                    </div>
                </form>
            </div>

        </section>
    </main>
</div>
@endsection