@extends('layouts.app')

@section('page_title')
{{"Register"}}
@endsection

@section('content')
<div id="container" class="container-sm d-flex justify-content-center">

    <main id="main" class="row">

        <section class="col" style="margin-top: 20%; width:586px">
            <div class="box p-5">

                <h1 class="fw-bolder">Login</h1>

                <form class="mt-5" action="{{ route('register') }}" method="POST">
                    @csrf
                    <!-- Username -->
                    <div class="mt-4">
                        <label class="form-label" for="name">Enter a name</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}" required autofocus>
                    </div>
                    <!-- Email -->
                    <div class="mt-4">
                        <label class="form-label" for="email">Your Email</label>
                        <input class="form-control" type="email" name="email" id="email" value="{{ old('email') }}" required>
                    </div>
                    <!-- Password -->
                    <div class="mt-4">
                        <label class="form-label" for="password">Your password</label>
                        <input class="form-control" type="password" name="password" id="password" value="{{ old('password') }}" required autocomplete="new-password">
                    </div>
                    <!-- Password confirmation -->
                    <div class="mt-4">
                        <label class="form-label" for="password_confirmation">Confirm your password</label>
                        <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" required>
                    </div>
                    <!-- Adress -->
                    <div class="mt-4">
                        <label class="form-label" for="adress">Your local adress </label>
                        <input class="form-control" type="text" name="adress" id="adress" required>
                    </div>
                    <!-- City -->
                    <div class="mt-4">
                        <label class="form-label" for="city">Town/City region</label>
                        <input class="form-control" type="text" name="city" id="city" required>
                    </div>

                    
                    <div class="row mt-5 d-flex justify-content-around">
                        <button type="submit" class="col-md-5 col-sm-12 btn greenBtn py-3 my-sm-2">Register</button>
                        <a class="col-md-5 col-sm-12 btn yellowBtn py-3 my-sm-2" href="{{ route('login') }}" >Already registered?</a>
                    </div>
                </form>
            </div>

        </section>
    </main>
</div>
@endsection