<!-- PAGE AJOUT DONATION -->

@extends('layouts.app')

@section('page_title')
{{"Add a donation"}}
@endsection

@section('content')
<div id="container" class="container-sm d-flex justify-content-center">

    <main id="main" class="row">

        <section class="col" style="margin-top: 20%; width:586px">
            <div class="box p-5">

                <h1 class="fw-bolder">Add a donation</h1>

                <x-success-message class="mb-4 mt-2" />

                <form class="mt-5" action="/add-donation" method="POST" enctype="multipart/form-data">
                    @csrf
                    <!-- Description -->
                    <div class="mt-4">
                        <label class="form-label" for="description">Enter a description</label>
                        <textarea class="form-control" name="description" id="description" cols="30" rows="5" style="resize: none;"></textarea>
                    </div>

                    <!-- Image -->
                    <div class="mt-4">
                        <label class="form-label" for="image">Insert an image</label>
                        <input class="form-control" type="file" name="image" id="image" accept="image/jpg, image/png">
                    </div>

                    <button type="submit" class="btn greenBtn mt-3">Create</button>

                </form>
            </div>

        </section>
    </main>
</div>
@endsection