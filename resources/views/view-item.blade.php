<!-- VOIR DONATION -->

@extends('layouts.app')

@section('page_title')
{{"View item"}}
@endsection

@section('content')

<div id="container">
    <main class="container-md" id="main">

                <div class="mt-5">
                    <article class="box text-white" style="width: auto;">
                        <h1 class="fw-bolder">View item</h1>
                    </article>

                    <section class="row justify-content-evenly mt-5 mx-sm-3">
                        
                        <article class="box col-md-7 col-sm-12 row p-3 mt-md-3 mt-sm-5 d-flex justify-content-around bg-red">
                            <img src="/storage/images/{{ $item->image }}" alt="image" class="col" style="border-radius: 100%; max-height: 200px; max-width: 200px;">
                            <div class="col">
                                <h3>Donation ID: {{ $item->id }}</h3>
                                <p style="text-align: justify;">{{ $item->description }}</p>
                            </div>
                        </article>

                    </section>

                    

                </div>
    </main>
</div>


@endsection