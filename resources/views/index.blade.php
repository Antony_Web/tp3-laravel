<!-- HOMEPAGE -->

@extends('layouts.app')

@section('page_title')
{{"Welcome"}}
@endsection

@section('content')


<div id="container">
    <main class="container-md" id="main">

                <section class="text-center text-white mt-5">
                    <x-simple-message class="mb-4 mt-2" />
                    <h1 class="fw-bolder">Welcome to the Laravel Donation center!</h1>
                    <h2 class="mt-5">On this website, we permit you to view, upload and reserve donation items at will, while seeing in real time the meteo of your local area!</h2>

                    <h4 class="mt-5 fw-bolder">To start using our web application, register or login into an account!</h4>

                    <div class="row mt-5">
                        <div class="gx-5">
                            <a class="col-md-3 btn col-sm-12 greenBtn" href="{{ route('login') }}"><button>Login</button></a>
                            <a class="col-md-3 btn col-sm-12 redBtn" href="{{ route('register') }}"><button>Register</button></a>
                        </div>
                    </div>
                </section>
    </main>
</div>

@endsection