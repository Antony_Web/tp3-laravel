<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'The inputs do not match our credentials or this account may not exist.',
    'password' => 'This password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
