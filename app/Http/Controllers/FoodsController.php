<?php

namespace App\Http\Controllers;

use App\Models\Foods;
use Illuminate\Http\Request;
use App\Models\User;
// use Intervention\Image\Facades\Image as InterventionImage;
// use Illuminate\Support\Facades\Storage  as Storage;

class FoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function browseDonations() // NAVIGUATION DES DONATIONS DISPONIBLES
    {
        //
        $donations = Foods::paginate(10);
        return view('browser', compact('donations'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function browseReserved() // NAVIGUATION DES DONATIONS RÉSERVÉS
    {
        //
        
        $user = User::with('food')->find(auth()->id());
        $reserved = $user->food;
        return view('profile', compact('reserved'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() // PAGE AJOUT DONATION
    {
        //
        return view('add-donation');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) // FONCTION AJOUT DE DONATION DANS LA DB
    {
        //

        $path = basename($request->image->store('images', 'public'));

        // $image = InterventionImage::make($request->image)->widen(200)->encode();
        // Storage::put('public/thumbs/' . $path, $image);

        $item = new Foods();
        $item->description = $request->description;
        $item->image = $path;
        $item->user_id = auth()->user()->id;
        $item->save();

        return redirect()->route('add-donation')->with('message', 'Donation item added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) // VOIR DONATION SINGULIÈRE
    {
        //
        $item = Foods::find($id);
        return view('view-item', compact('item'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     */
    public function reserve(Request $request, $id) // FONCTION RÉSERVER UNE DONATION
    {

        //Foods::find($id)->reserve($request->only('is_reserved'));
        $resitem = Foods::find($id);
        $resitem->is_reserved = $request->reserve;
        $resitem->save();
        $user = User::find(auth()->user()->id);
        $user->food_id = $id;
        $user->save();

        return redirect()->route('profile', compact('resitem'))->with('message', 'Donation reserved successfully.');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) // CONFIRMER LE RAMASSAGE DE DONATION
    {
        //
        $user = auth()->user();
        $user->food_id = null;


        $foodDel = Foods::find($id);
        $foodDel->delete();

        return redirect()->route('profile')->with('message', 'Donation confirmed for pick up.');
    }
}