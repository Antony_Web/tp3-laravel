<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //Update profile

    public function update(UpdateProfileRequest $request){ // METTRE À JOUR LE PROFIL

        auth()->user()->update($request->only('name', 'email', 'adress', 'city'));

        if($request->input('password')) {
            auth()->user()->update([
                'password' => bcrypt($request->input('password'))
            ]);
        }

        return redirect()->route('profile')->with('message', 'Profile updated successfully.');
    }
}
