<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::group(['middleware' => 'auth'], function() {

    Route::get('/browser', [\App\Http\Controllers\FoodsController::class, 'browseDonations']);
    Route::put('/browser/{id}', [\App\Http\Controllers\FoodsController::class, 'reserve'])->name('donation.reserve');

    Route::get('/browser/food={id}', [\App\Http\Controllers\FoodsController::class, 'show'])->name('view-item');

    Route::get('/add-donation', [\App\Http\Controllers\FoodsController::class, 'create'])->name('add-donation');
    Route::post('/add-donation', [\App\Http\Controllers\FoodsController::class, 'store'])->name('add-donation');

    Route::put('profile', [\App\Http\Controllers\ProfileController::class, 'update'])->name('profile.update');
    Route::get('/profile', [\App\Http\Controllers\FoodsController::class, 'browseReserved'])->name('profile');
    Route::delete('/profile/{id}', [\App\Http\Controllers\FoodsController::class, 'destroy'])->name('item.confirm');
});


require __DIR__.'/auth.php';