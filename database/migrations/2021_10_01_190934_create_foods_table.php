<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('foods', function (Blueprint $table) {
            $table->bigIncrements('id')->length(20);
            $table->string('description', 255);
            $table->string('image', 255);
            $table->integer('meteo')->length(11)->default(0);
            $table->bigInteger('user_id')->length(20)->unsigned()->nullable();
            $table->tinyInteger('is_reserved')->length(1)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET GLOBAL FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('foods');
        DB::statement('SET GLOBAL FOREIGN_KEY_CHECKS=1;');
    }
}